## usb udev hunting

    man udev

I'm new to linux usb so this is a small look


I have a usb device that has a live device on the end.
Wanting to talk to it with code.  python maybe


device may connect to this device enpoint. this is how
it will be referenced in code. generically a ttyUSB0.

    /dev/ttyUSB0

what if your unsure if the usb is getting seen.
or is a permissions issue.


### dmesg or journalctl

    man dmesg

use these two logging tools to find messages about device errors/activity/connection.
dmesg prints the ring buffer and is an in memory constant sized log.

filter messages with usb in the text.

    dmesg | grep usb

    dmesg | grep ttyUSB*

this should produce the same results as dmesg

        journalctl --dmesg


### look for device

if unsure use `lsusb` to see if device is seen by os.

    $lsusb

    ...
    Bus 003 Device 002: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter
    ...

now trying to identify if the os routed the device to a tty enpoint

first identify the bus/device path

here is this usb bus and device based on output from `lsusb`

bus = 003 
device = 002

now create a path to the logical device

    $ll /dev/bus/usb/003/002

    crw-rw-r-- 1 root dialout 189, 257 Nov 13 18:00 /dev/bus/usb/003/002


the os sees the device and has created a path for it.

now get other info


    $udevadm info -q path -n /dev/bus/usb/003/002

    /devices/pci0000:00/0000:00:12.0/usb3/3-1

here you can see the device path to the hardware pci

    $udevadm info -a -p $(udevadm info -q path -n /dev/bus/usb/003/002)


    Udevadm info starts with the device specified by the devpath and then
    walks up the chain of parent devices. It prints for every device
    found, all possible attributes in the udev rules key format.
    A rule to match, can be composed by the attributes of the device
    and the attributes from one single parent device.

      looking at device '/devices/pci0000:00/0000:00:12.0/usb3/3-1':
        KERNEL=="3-1"
        SUBSYSTEM=="usb"
        DRIVER=="usb"
        ATTR{authorized}=="1"
        ...

if you got this far then the device is seen by the os.


### talk to the device with bash

use echo and cat to talk to device

    $echo "some kind of command" > /dev/ttyUSB0

    $cat -v < /dev/ttyUSB0

did something print out? maybe an error message like

    permission denied

or

    /dev/ttyUSB0: No such file or directory


### user permissions

not sure if user permissions should be altered first or a rule composed.


when you 

    $ll /dev/ttyUSB0
    
    crw-rw---- 1 root dialout 188, 0 Nov 13 20:53 /dev/ttyUSB0

look at the user and group assignments. are you a member 
of the group dialout?  the acting process user.

    $members dialout

did it list the user? if not add the user to the group

    sudo adduser you dialout

log out and log back in. test the device by talking to it.

to remove the user from the dialout group

    deluser user dialout


### rules

    man udev

a systemd service that sends messages from devices.
there is a layer or permissions that need to be met.

create a rule in `/etc/udev/rules.d/`  there are instructions
in `man udev`.  `lsub` and `udevadm` have the parts you will
need for a permission rule.

    Bus 003 Device 002: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter

create a file `/etc/udev/rules.d/10-local.rules` and put
a line like this.  

    SUBSYSTEM=="usb", ATTR{idVendor}=="1a86", MODE="0666", GROUP="dialout"

notice the "1a86" device vendor id and the group from earlier.

now try testing/simulating the rule.

    $udevadm test /devices/pci0000:00/0000:00:12.0/usb3/3-1

    calling: test
    version 237
    This program is for debugging only, it does not run any program
    specified by a RUN key. It may show incorrect results, because
    some values may be different, or not available at a simulation run.

    Load module index
    Parsed configuration file /lib/systemd/network/99-default.link
    Created link configuration context.
    Reading rules file: /etc/udev/rules.d/10-local.rules
    Reading rules file: /lib/udev/rules.d/39-usbmuxd.rules
    Reading rules file: /lib/udev/rules.d/40-usb-media-players.rules
    ...


did it load the rules file?
does it see your device?


### links

[udev](http://weininger.net/how-to-write-udev-rules-for-usb-devices.html)

